# How to run

### Run app, redis and webserver on localhost:80

```
docker-compose up -d webserver
```

API endpoints:
- PUT /visits/{countryCode}
- GET /visits/statistics

For development uncomment 
- **volumes** section in **app** container 
- arg **DEVBUILD=TRUE** in **app** container

in docker-compose.yml file

###  run tests

```
docker-compose up phpunit
```

###  run benchmark

#### visits

```
docker-compose up k6-visit
```
#### statistics

```
docker-compose up k6-statistics
```

# Benchmarks

Hardware: macbook pro 2,9 GHz 4‑core Intel Core i7 with 16 Gb 2133 MHz LPDDR3

### visit

#### 10 seconds
```
data_received..................: 2.0 MB 182 kB/s
data_sent......................: 887 kB 80 kB/s
http_req_blocked...............: avg=8.86ms   min=1.34µs  med=3.73µs  max=210.53ms p(90)=1.43ms   p(95)=92.58ms
http_req_connecting............: avg=8.04ms   min=0s      med=0s      max=159.94ms p(90)=798.27µs p(95)=91.12ms
http_req_duration..............: avg=195.68ms min=2.72ms  med=66.21ms max=1.64s    p(90)=408.95ms p(95)=1.11s   
  { expected_response:true }...: avg=195.68ms min=2.72ms  med=66.21ms max=1.64s    p(90)=408.95ms p(95)=1.11s   
http_req_failed................: 0.00%  ✓ 0          ✗ 8613  
http_req_receiving.............: avg=115.94µs min=13.17µs med=66.69µs max=10.17ms  p(90)=234.93µs p(95)=331.37µs
http_req_sending...............: avg=3.31ms   min=4.51µs  med=13.94µs max=173.89ms p(90)=441.99µs p(95)=28.9ms  
http_req_tls_handshaking.......: avg=0s       min=0s      med=0s      max=0s       p(90)=0s       p(95)=0s      
http_req_waiting...............: avg=192.25ms min=2.68ms  med=66.06ms max=1.6s     p(90)=401ms    p(95)=1.1s    
http_reqs......................: 8613   780.106377/s
iteration_duration.............: avg=1.2s     min=1s      med=1.06s   max=2.78s    p(90)=1.49s    p(95)=2.12s   
iterations.....................: 8613   780.106377/s
vus............................: 453    min=453      max=1000
vus_max........................: 1000   min=1000     max=1000
```

#### 30 seconds
```
data_received..................: 4.9 MB 154 kB/s
data_sent......................: 2.2 MB 68 kB/s
http_req_blocked...............: avg=14.32ms  min=1.26µs  med=4.1µs    max=454.26ms p(90)=9.08µs   p(95)=208µs   
http_req_connecting............: avg=8.77ms   min=0s      med=0s       max=339.28ms p(90)=0s       p(95)=0s      
http_req_duration..............: avg=448.78ms min=2.85ms  med=135.48ms max=2.19s    p(90)=1.07s    p(95)=1.12s   
  { expected_response:true }...: avg=448.78ms min=2.85ms  med=135.48ms max=2.19s    p(90)=1.07s    p(95)=1.12s   
http_req_failed................: 0.00%  ✓ 0          ✗ 21158 
http_req_receiving.............: avg=129.15µs min=12.44µs med=74.93µs  max=14.81ms  p(90)=275.27µs p(95)=379.09µs
http_req_sending...............: avg=3.14ms   min=4.52µs  med=15.24µs  max=195.65ms p(90)=81.81µs  p(95)=531.02µs
http_req_tls_handshaking.......: avg=0s       min=0s      med=0s       max=0s       p(90)=0s       p(95)=0s      
http_req_waiting...............: avg=445.51ms min=2.72ms  med=135.4ms  max=2.19s    p(90)=1.07s    p(95)=1.11s   
http_reqs......................: 21158  660.850779/s
iteration_duration.............: avg=1.46s    min=1s      med=1.13s    max=3.28s    p(90)=2.08s    p(95)=2.14s   
iterations.....................: 21158  660.850779/s
vus............................: 249    min=249      max=1000
vus_max........................: 1000   min=1000     max=1000
```

### statistics

#### 10 seconds
```
data_received..................: 2.1 MB 178 kB/s
data_sent......................: 789 kB 67 kB/s
http_req_blocked...............: avg=9.79ms   min=1.08µs  med=3.77µs   max=205.65ms p(90)=65.79ms  p(95)=82.88ms
http_req_connecting............: avg=9.5ms    min=0s      med=0s       max=163.29ms p(90)=65.47ms  p(95)=82.5ms  
http_req_duration..............: avg=252.89ms min=2.82ms  med=155.73ms max=1.63s    p(90)=586.96ms p(95)=741.3ms
  { expected_response:true }...: avg=252.89ms min=2.82ms  med=155.73ms max=1.63s    p(90)=586.96ms p(95)=741.3ms
http_req_failed................: 0.00%  ✓ 0          ✗ 8573  
http_req_receiving.............: avg=123.22µs min=13.98µs med=74.38µs  max=8.41ms   p(90)=253.14µs p(95)=362.84µs
http_req_sending...............: avg=3.4ms    min=4.61µs  med=14.23µs  max=78.14ms  p(90)=6.56ms   p(95)=29.56ms
http_req_tls_handshaking.......: avg=0s       min=0s      med=0s       max=0s       p(90)=0s       p(95)=0s      
http_req_waiting...............: avg=249.36ms min=2.74ms  med=155.45ms max=1.59s    p(90)=570.11ms p(95)=740.5ms
http_reqs......................: 8573   730.953771/s
iteration_duration.............: avg=1.26s    min=1s      med=1.15s    max=2.73s    p(90)=1.61s    p(95)=1.79s   
iterations.....................: 8573   730.953771/s
vus............................: 62     min=62       max=1000
vus_max........................: 1000   min=1000     max=1000
```

#### 30 seconds

```
data_received..................: 5.4 MB 164 kB/s
data_sent......................: 2.0 MB 62 kB/s
http_req_blocked...............: avg=6.8ms    min=1.04µs  med=3.88µs  max=230.91ms p(90)=7.21µs  p(95)=115.18µs
http_req_connecting............: avg=3.08ms   min=0s      med=0s      max=100.48ms p(90)=0s      p(95)=0s      
http_req_duration..............: avg=423.02ms min=2.75ms  med=76.11ms max=3.09s    p(90)=1.06s   p(95)=1.19s   
  { expected_response:true }...: avg=423.02ms min=2.75ms  med=76.11ms max=3.09s    p(90)=1.06s   p(95)=1.19s   
http_req_failed................: 0.00%  ✓ 0         ✗ 21886 
http_req_receiving.............: avg=123.67µs min=13.28µs med=74.88µs max=8.02ms   p(90)=263µs   p(95)=355.97µs
http_req_sending...............: avg=708.4µs  min=4.55µs  med=14.26µs max=55.61ms  p(90)=71.71µs p(95)=348.11µs
http_req_tls_handshaking.......: avg=0s       min=0s      med=0s      max=0s       p(90)=0s      p(95)=0s      
http_req_waiting...............: avg=422.19ms min=2.7ms   med=75.98ms max=3.09s    p(90)=1.06s   p(95)=1.17s   
http_reqs......................: 21886  670.57182/s
iteration_duration.............: avg=1.43s    min=1s      med=1.07s   max=4.09s    p(90)=2.06s   p(95)=2.29s   
iterations.....................: 21886  670.57182/s
vus............................: 392    min=392     max=1000
vus_max........................: 1000   min=1000    max=1000
```
