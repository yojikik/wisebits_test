<?php

namespace Tests\App\Http\Controllers;

use App\Services\VisitsRepository\VisitsRepository;
use Tests\TestCase;

class VisitsControllerTest extends TestCase
{
    public function provider_testStatistics()
    {
        return [
            'valid statistic' => [
                'statistics' => [
                    'CY' => 1,
                    'RU' => 5,
                ],
                'expected' => '{"CY":1,"RU":5}',
            ],
            'empty statistic' => [
                'statistics' => [],
                'expected' => '{}',
            ]
        ];
    }

    /**
     * @dataProvider provider_testStatistics
     */
    public function testStatistics($statistics, $expected)
    {
        $repository = $this->getMockBuilder(VisitsRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $repository->expects($this->once())
            ->method('statistics')
            ->willReturn($statistics);

        $this->app->instance(VisitsRepository::class, $repository);
        $response = $this->call('GET', '/visits/statistics');
        $this->assertEquals($expected, $response->content());
    }


    public function provider_testVisit()
    {
        return [
            'valid' => [
                'country' => 'CY',
                'isCountryAvailable' => true,
                'visitSuccess' => true,
                'expectedStatus' => 200,
            ],
            'country is not available' => [
                'country' => 'CY',
                'isCountryAvailable' => false,
                'visitSuccess' => true,
                'expectedStatus' => 422,
            ],
            'visit not success' => [
                'country' => 'CY',
                'isCountryAvailable' => true,
                'visitSuccess' => false,
                'expectedStatus' => 500,
            ],
        ];
    }

    /**
     * @dataProvider provider_testVisit
     */
    public function testVisit(
        $country,
        $isCountryAvailable,
        $visitSuccess,
        $expectedStatus,
    )
    {
        $repository = $this->getMockBuilder(VisitsRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $repository->expects($this->once())
            ->method('getAvailableCountries')
            ->willReturn($isCountryAvailable ? [$country => $country] : []);

        $repository->expects($isCountryAvailable ? $this->once() : $this->never())
            ->method('visit')
            ->willReturn($visitSuccess);

        $this->app->instance(VisitsRepository::class, $repository);
        $response = $this->call('PUT', '/visits/' . $country);
        $this->assertEquals($expectedStatus, $response->status());
    }
}
