<?php
declare(strict_types=1);

namespace Tests\App\Services\VisitsRepository;

use App\Services\VisitsRepository\VisitsRepository;
use Tests\TestCase;

class VisitsRepositoryTest extends TestCase
{
    public function testGetAvailableCountries()
    {
        $repository = $this
            ->getMockBuilder(VisitsRepository::class)
            ->onlyMethods([
                'visit',
                'statistics'
            ])
            ->getMock();
        $result = $repository->getAvailableCountries();
        $this->assertEquals(VisitsRepository::AVAILABLE_COUNTRIES, $result);
    }
}
