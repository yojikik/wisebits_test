<?php
declare(strict_types=1);

namespace Tests\App\Services\VisitsRepository;

use App\Services\VisitsRepository\RedisVisitsRepository;
use Tests\TestCase;

class RedisVisitsRepositoryTest extends TestCase
{
    public function testVisit()
    {
        $countryCode = '_TEST_COUNTRY_';
        $redisMock = \Mockery::mock('redis');
        $redisMock->shouldReceive('hIncrBy')
            ->once()
            ->with(
                RedisVisitsRepository::REDIS_KEY,
                $countryCode,
                1,
            )
            ->andReturn(true);

        $this->app->instance('redis', $redisMock);

        $repository = $this
            ->getMockBuilder(RedisVisitsRepository::class)
            ->addMethods([])
            ->getMock();
        $result = $repository->visit($countryCode);
        $this->assertTrue($result);
    }

    public function testStatistics()
    {
        $expected = ['_TEST_RESPONSE_'];
        $redisMock = \Mockery::mock('redis');
        $redisMock->shouldReceive('hGetAll')
            ->once()
            ->with(RedisVisitsRepository::REDIS_KEY)
            ->andReturn($expected);

        $this->app->instance('redis', $redisMock);

        $repository = $this
            ->getMockBuilder(RedisVisitsRepository::class)
            ->addMethods([])
            ->getMock();
        $result = $repository->statistics();
        $this->assertEquals($expected, $result);
    }
}

