<?php

namespace App\Exceptions\Response;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SomethingWentWrong extends Exception
{
    protected function getDefaultCode(): int
    {
        return 500;
    }
}
