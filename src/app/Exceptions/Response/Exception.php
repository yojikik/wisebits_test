<?php

namespace App\Exceptions\Response;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Exception extends HttpException
{
    public function __construct(
        protected ?array $data = null,
        ?int $statusCode = null,
        string $message = '',
        \Throwable $previous = null,
        array $headers = []
    )
    {
        $statusCode = $statusCode ?? $this->getDefaultCode();
        $this->headers = $headers;

        parent::__construct($statusCode, $message, $previous);
    }

    protected function getDefaultCode(): int
    {
        return 400;
    }

    public function render(): JsonResponse
    {
        return new JsonResponse(
            $this->getResponseMessage(),
            $this->getStatusCode(),
            $this->getHeaders(),
            JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
        );
    }

    private function getResponseMessage(): ?array
    {
        return $this->getData();
    }

    protected function getData(): ?array
    {
        return $this->data;
    }

}
