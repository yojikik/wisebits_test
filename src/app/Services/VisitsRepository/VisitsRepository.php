<?php
declare(strict_types=1);

namespace App\Services\VisitsRepository;

abstract class VisitsRepository
{
    const AVAILABLE_COUNTRIES = [
        'AB' => 'AB', 'AU' => 'AU', 'AT' => 'AT', 'AZ' => 'AZ', 'AL' => 'AL', 'DZ' => 'DZ', 'AS' => 'AS', 'AI' => 'AI', 'AO' => 'AO', 'AD' => 'AD', 'AQ' => 'AQ', 'AG' => 'AG', 'AR' => 'AR', 'AM' => 'AM', 'AW' => 'AW', 'AF' => 'AF', 'BS' => 'BS', 'BD' => 'BD', 'BB' => 'BB', 'BH' => 'BH', 'BY' => 'BY', 'BZ' => 'BZ', 'BE' => 'BE', 'BJ' => 'BJ', 'BM' => 'BM', 'BG' => 'BG', 'BO' => 'BO', 'BQ' => 'BQ', 'BA' => 'BA', 'BW' => 'BW', 'BR' => 'BR', 'IO' => 'IO', 'BN' => 'BN', 'BF' => 'BF', 'BI' => 'BI', 'BT' => 'BT', 'VU' => 'VU', 'HU' => 'HU', 'VE' => 'VE', 'VG' => 'VG', 'VI' => 'VI', 'VN' => 'VN', 'GA' => 'GA', 'HT' => 'HT', 'GY' => 'GY', 'GM' => 'GM', 'GH' => 'GH', 'GP' => 'GP', 'GT' => 'GT', 'GN' => 'GN', 'GW' => 'GW', 'DE' => 'DE', 'GG' => 'GG', 'GI' => 'GI', 'HN' => 'HN', 'HK' => 'HK', 'GD' => 'GD', 'GL' => 'GL', 'GR' => 'GR', 'GE' => 'GE', 'GU' => 'GU', 'DK' => 'DK', 'JE' => 'JE', 'DJ' => 'DJ', 'DM' => 'DM', 'DO' => 'DO', 'EG' => 'EG', 'ZM' => 'ZM', 'EH' => 'EH', 'ZW' => 'ZW', 'IL' => 'IL', 'IN' => 'IN', 'ID' => 'ID', 'JO' => 'JO', 'IQ' => 'IQ', 'IR' => 'IR', 'IE' => 'IE', 'IS' => 'IS', 'ES' => 'ES', 'IT' => 'IT', 'YE' => 'YE', 'CV' => 'CV', 'KZ' => 'KZ', 'KH' => 'KH', 'CM' => 'CM', 'CA' => 'CA', 'QA' => 'QA', 'KE' => 'KE', 'CY' => 'CY', 'KG' => 'KG', 'KI' => 'KI', 'CN' => 'CN', 'CC' => 'CC', 'CO' => 'CO', 'KM' => 'KM', 'CG' => 'CG', 'CD' => 'CD', 'KP' => 'KP', 'KR' => 'KR', 'CR' => 'CR', 'CI' => 'CI', 'CU' => 'CU', 'KW' => 'KW', 'CW' => 'CW', 'LA' => 'LA', 'LV' => 'LV', 'LS' => 'LS', 'LB' => 'LB', 'LY' => 'LY', 'LR' => 'LR', 'LI' => 'LI', 'LT' => 'LT', 'LU' => 'LU', 'MU' => 'MU', 'MR' => 'MR', 'MG' => 'MG', 'YT' => 'YT', 'MO' => 'MO', 'MW' => 'MW', 'MY' => 'MY', 'ML' => 'ML', 'UM' => 'UM', 'MV' => 'MV', 'MT' => 'MT', 'MA' => 'MA', 'MQ' => 'MQ', 'MH' => 'MH', 'MX' => 'MX', 'FM' => 'FM', 'MZ' => 'MZ', 'MD' => 'MD', 'MC' => 'MC', 'MN' => 'MN', 'MS' => 'MS', 'MM' => 'MM', 'NA' => 'NA', 'NR' => 'NR', 'NP' => 'NP', 'NE' => 'NE', 'NG' => 'NG', 'NL' => 'NL', 'NI' => 'NI', 'NU' => 'NU', 'NZ' => 'NZ', 'NC' => 'NC', 'NO' => 'NO', 'AE' => 'AE', 'OM' => 'OM', 'BV' => 'BV', 'IM' => 'IM', 'NF' => 'NF', 'CX' => 'CX', 'HM' => 'HM', 'KY' => 'KY', 'CK' => 'CK', 'TC' => 'TC', 'PK' => 'PK', 'PW' => 'PW', 'PS' => 'PS', 'PA' => 'PA', 'VA' => 'VA', 'PG' => 'PG', 'PY' => 'PY', 'PE' => 'PE', 'PN' => 'PN', 'PL' => 'PL', 'PT' => 'PT', 'PR' => 'PR', 'MK' => 'MK', 'RE' => 'RE', 'RU' => 'RU', 'RW' => 'RW', 'RO' => 'RO', 'WS' => 'WS', 'SM' => 'SM', 'ST' => 'ST', 'SA' => 'SA', 'SH' => 'SH', 'MP' => 'MP', 'BL' => 'BL', 'MF' => 'MF', 'SN' => 'SN', 'VC' => 'VC', 'KN' => 'KN', 'LC' => 'LC', 'PM' => 'PM', 'RS' => 'RS', 'SC' => 'SC', 'SG' => 'SG', 'SX' => 'SX', 'SY' => 'SY', 'SK' => 'SK', 'SI' => 'SI', 'GB' => 'GB', 'US' => 'US', 'SB' => 'SB', 'SO' => 'SO', 'SD' => 'SD', 'SR' => 'SR', 'SL' => 'SL', 'TJ' => 'TJ', 'TH' => 'TH', 'TW' => 'TW', 'TZ' => 'TZ', 'TL' => 'TL', 'TG' => 'TG', 'TK' => 'TK', 'TO' => 'TO', 'TT' => 'TT', 'TV' => 'TV', 'TN' => 'TN', 'TM' => 'TM', 'TR' => 'TR', 'UG' => 'UG', 'UZ' => 'UZ', 'UA' => 'UA', 'WF' => 'WF', 'UY' => 'UY', 'FO' => 'FO', 'FJ' => 'FJ', 'PH' => 'PH', 'FI' => 'FI', 'FK' => 'FK', 'FR' => 'FR', 'GF' => 'GF', 'PF' => 'PF', 'TF' => 'TF', 'HR' => 'HR', 'CF' => 'CF', 'TD' => 'TD', 'ME' => 'ME', 'CZ' => 'CZ', 'CL' => 'CL', 'CH' => 'CH', 'SE' => 'SE', 'SJ' => 'SJ', 'LK' => 'LK', 'EC' => 'EC', 'GQ' => 'GQ', 'AX' => 'AX', 'SV' => 'SV', 'ER' => 'ER', 'SZ' => 'SZ', 'EE' => 'EE', 'ET' => 'ET', 'ZA' => 'ZA', 'GS' => 'GS', 'OS' => 'OS', 'SS' => 'SS', 'JM' => 'JM', 'JP' => 'JP'
    ];

    public function getAvailableCountries(): array
    {
        return static::AVAILABLE_COUNTRIES;
    }

    abstract public function visit(string $countryCode): bool;

    abstract public function statistics(): array;
}
