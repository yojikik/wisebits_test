<?php
declare(strict_types=1);

namespace App\Services\VisitsRepository;

class RedisVisitsRepository extends VisitsRepository
{
    const REDIS_KEY = 'visits';
    protected $redis;

    public function __construct()
    {
        $this->redis = app('redis');
    }

    public function visit(string $countryCode): bool
    {
        return (bool) $this->redis->hIncrBy(static::REDIS_KEY, $countryCode, 1);
    }

    public function statistics(): array
    {
        return $this->redis->hGetAll(static::REDIS_KEY);
    }
}
