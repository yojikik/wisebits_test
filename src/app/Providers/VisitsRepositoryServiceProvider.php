<?php
declare(strict_types=1);

namespace App\Providers;

use App\Services\VisitsRepository\RedisVisitsRepository;
use App\Services\VisitsRepository\VisitsRepository;
use Illuminate\Support\ServiceProvider;

class VisitsRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(VisitsRepository::class, function () {
            return new RedisVisitsRepository();
        });
    }
}
