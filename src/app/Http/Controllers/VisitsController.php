<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Exceptions\Response\SomethingWentWrong;
use App\Exceptions\Response\Unprocessable;
use App\Services\VisitsRepository\VisitsRepository;
use Illuminate\Http\JsonResponse;

class VisitsController extends Controller
{
    public function visit(string $countryCode, VisitsRepository $visitsService): JsonResponse
    {
        if (!array_key_exists($countryCode, $visitsService->getAvailableCountries())) {
            throw new Unprocessable();
        }

        if (!$visitsService->visit($countryCode)) {
            throw new SomethingWentWrong();
        }

        return response()->json();
    }

    public function statistics(VisitsRepository $visitsService): JsonResponse
    {
        $statistics = $visitsService->statistics();
        if (empty($statistics)) {
            $statistics = new \stdClass();
        }
        return response()->json($statistics);
    }
}
