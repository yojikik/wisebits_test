# базовый образ
FROM php:8.1-fpm-buster

RUN apt-get update && apt-get install -y \
        libzip-dev \
        zip \
    && docker-php-ext-install zip \
    && docker-php-ext-install opcache \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /var/www/html
COPY ./src/composer.* /var/www/html/
RUN composer install --no-autoloader
COPY ./src /var/www/html/
RUN composer dump-autoload

ARG DEVBUILD
ARG MAXCHILDREN
RUN { \
        if [ ${DEVBUILD} = "TRUE" ]; \
           then echo 'opcache.enable=0'; \
           else echo 'opcache.validate_timestamps=0'; \
        fi \
        } > /usr/local/etc/php/conf.d/opcache-recommended.ini && \
        echo "pm.max_children = ${MAXCHILDREN}" >> /usr/local/etc/php-fpm.d/www.conf && \
        echo "pm.start_servers = ${MAXCHILDREN}" >> /usr/local/etc/php-fpm.d/www.conf && \
        echo "pm.min_spare_servers = $((${MAXCHILDREN} / 2))" >> /usr/local/etc/php-fpm.d/www.conf && \
        echo "pm.max_spare_servers = ${MAXCHILDREN}" >> /usr/local/etc/php-fpm.d/www.conf
